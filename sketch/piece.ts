
enum PieceTypes {
    NONE,
    PAWN,
    ROOK,
    KNIGHT,
    BISHOP,
    QUEEN,
    KING
}

enum Color {
    NONE,
    WHITE,
    BLACK
}

class Piece {
    type: PieceTypes
    color: Color
    firstMove: boolean
    constructor(type: PieceTypes, color: Color = Color.NONE) {
        this.type = type
        this.color = color
        this.firstMove = true
    }

    draw(inx: number, iny: number, scale: number = 1) {
        if(this.type == PieceTypes.NONE) {
            return 
        }

        if(this.color == Color.WHITE) {
            fill(255) 
        }
        else {
            fill(50) 
        }

        stroke(50)
        strokeWeight(2)
        circle(inx + 50 *scale, iny + 50* scale, 80 * scale)
        noStroke();
         

        let x = inx + 35 * scale;
        let y = iny + 65 * scale;
        textSize(50 * scale)

        if(this.color == Color.WHITE) {
            fill(0) 
        }
        else {
            fill(255) 
        }

        switch(this.type) {
            case PieceTypes.PAWN:
                text("P", x, y);
                break;
            case PieceTypes.ROOK:
                text("R", x, y);
                break;
            case PieceTypes.KNIGHT:
                text("N", x, y);
                break;
            case PieceTypes.BISHOP:
                text("B", x, y);
                break;
            case PieceTypes.KING:
                text("K", x, y);
                break;
            case PieceTypes.QUEEN:
                text("Q", x, y);
                break;
        }
    }
    
    getLongMoves(pos: Pos, dir: Pos, board: Array<Piece>) : Array<Move> {
        let ret: Array<Move> = [];
        
        let tx = pos.x + dir.x;
        let ty = pos.y + dir.y;
        let piece: Piece;
        while(true) {
            if(isOutOfBounds(tx, ty)) {
                break; 
            }
            
            piece = board[index(tx,ty)]
            if(piece.color === this.color) {
                break;
            }

            if(piece.type != PieceTypes.NONE) {
                ret.push(<Move>{
                    x: tx,
                    y: ty,
                    type: MoveType.TAKE 
                }) 
                break;
            }

            ret.push(<Move>{
                x: tx,
                y: ty,
                type: MoveType.NORMAL 
            }) 
            
            ty += dir.y;
            tx += dir.x;
        }

        return ret;
    }


    getNeighborMove(pos: Pos, dir: Pos, board: Array<Piece>) : Array<Move>{
        if(!isOutOfBounds(pos.x + dir.x, pos.y + dir.y)) {
            let piece = board[index(pos.x + dir.x, pos.y + dir.y)]

            if(piece.type == PieceTypes.NONE) {
                return [<Move>{
                    x: pos.x + dir.x,
                    y: pos.y + dir.y,
                    type: MoveType.NORMAL
                }];
            }
            else if(piece.color != this.color) {
                return [<Move>{
                    x: pos.x + dir.x,
                    y: pos.y + dir.y,
                    type: MoveType.TAKE
                }]; 
            }
        }

        return []
    }

    getPossibleMoves(x: number, y: number, board: Array<Piece>): Array<Move> {
        let ret: Array<Move> = []
        switch(this.type) {
            case PieceTypes.NONE:
                return ret;

            case PieceTypes.PAWN:
                if(this.color == Color.WHITE) {

                    // move one up
                    if(!isOutOfBounds(x, y -1) && board[index(x, y - 1)].type == PieceTypes.NONE) {
                        ret.push(<Move>{
                            x, y: y - 1, type: MoveType.NORMAL 
                        }) 
                        
                        // move two up on the first move
                        if(this.firstMove && !isOutOfBounds(x, y - 2) && board[index(x, y - 2)].type == PieceTypes.NONE) {
                            ret.push(<Move>{
                                x, y: y - 2, type: MoveType.NORMAL 
                            }) 
                        }
                    }

                    // take left
                    if(!isOutOfBounds(x - 1, y - 1) && board[index(x - 1, y - 1)].color == Color.BLACK) {
                        ret.push(<Move>{
                            x: x - 1,
                            y: y - 1,
                            type: MoveType.TAKE 
                        }) 
                    }
                        

                    // take right
                    if(!isOutOfBounds(x + 1, y - 1) && board[index(x + 1, y - 1)].color == Color.BLACK) {
                        ret.push(<Move>{
                            x: x + 1,
                            y: y - 1,
                            type: MoveType.TAKE 
                        }) 
                    }
                    
                }
                else {
                    // move one down
                    if(!isOutOfBounds(x, y + 1) && board[index(x, y + 1)].type == PieceTypes.NONE) {
                        ret.push(<Move>{
                            x, y: y + 1, type: MoveType.NORMAL 
                        }) 
                        
                        // move two down on the first move
                        if(this.firstMove && !isOutOfBounds(x, y + 2) && board[index(x, y + 2)].type == PieceTypes.NONE) {
                            ret.push(<Move>{
                                x, y: y + 2, type: MoveType.NORMAL 
                            }) 
                        }
                    }

                    // take left
                    if(!isOutOfBounds(x - 1, y + 1) && board[index(x - 1, y + 1)].color == Color.WHITE) {
                        ret.push(<Move>{
                            x: x - 1,
                            y: y + 1,
                            type: MoveType.TAKE 
                        }) 
                    }
                        

                    // take right
                    if(!isOutOfBounds(x + 1, y + 1) && board[index(x + 1, y + 1)].color == Color.WHITE) {
                        ret.push(<Move>{
                            x: x + 1,
                            y: y + 1,
                            type: MoveType.TAKE 
                        }) 
                    }
                }

                break;
            case PieceTypes.KNIGHT:
                
                // up left
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:-1,y:-2}, board))

                // up right
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:+1,y:-2}, board))

                // left up
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:-2,y:-1}, board))

                // left down
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:-2,y:+1}, board))
                
                // right up
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:+2,y:-1}, board))

                // right down
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:+2,y:+1}, board))
                
                // down left
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:-1,y:+2}, board))

                // down right 
                ret.push(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x:+1,y:+2}, board))
                break;
                

            case PieceTypes.ROOK: {
                // up
                let moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:0, y:-1}, board)
                ret = ret.concat(...moves)

                // right
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:+1, y:0}, board)
                ret = ret.concat(...moves)

                // down
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:0, y:+1}, board)
                ret = ret.concat(...moves)

                // left
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:-1, y:0}, board)
                ret = ret.concat(...moves)

                break;
            }
            case PieceTypes.BISHOP:{
                // up left
                let moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:-1, y:-1}, board)
                ret = ret.concat(...moves)
                // up right
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:+1, y:-1}, board)
                ret = ret.concat(...moves)

                // down left
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:-1, y:+1}, board)
                ret = ret.concat(...moves)

                // down right
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:+1, y:+1}, board)
                ret = ret.concat(...moves)
                break;
            }
            case PieceTypes.QUEEN: {
            
                // up
                let moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:0, y:-1}, board)
                ret = ret.concat(...moves)

                // right
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:+1, y:0}, board)
                ret = ret.concat(...moves)

                // down
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:0, y:+1}, board)
                ret = ret.concat(...moves)

                // left
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:-1, y:0}, board)
                ret = ret.concat(...moves)
                // up left
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:-1, y:-1}, board)
                ret = ret.concat(...moves)
                // up right
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:+1, y:-1}, board)
                ret = ret.concat(...moves)

                // down left
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:-1, y:+1}, board)
                ret = ret.concat(...moves)

                // down right
                moves = this.getLongMoves(<Pos>{x,y}, <Pos>{x:+1, y:+1}, board)
                ret = ret.concat(...moves)
                break;
            }

            case PieceTypes.KING:
                
                // up
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: +0,y: -1}, board))

                // down
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: +0,y: +1}, board))

                // left
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: -1,y: 0}, board))

                // right 
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: +1,y: 0}, board))

                // up left
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: -1,y: -1}, board))

                // up right
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: +1,y: -1}, board))

                // down left
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: -1,y: +1}, board))

                // down right
                ret = ret.concat(...this.getNeighborMove(<Pos>{x,y}, <Pos>{x: +1,y: +1}, board))

                // castle kingside
                if(this.firstMove && 
                   board[index(x + 1, y)].type == PieceTypes.NONE &&
                   board[index(x + 2, y)].type == PieceTypes.NONE &&
                   board[index(x + 3, y)].type == PieceTypes.ROOK &&
                   board[index(x + 3, y)].firstMove) {
                    ret.push(<Move>{
                        x: x + 2,
                        y: y,
                        type: MoveType.CASTLE 
                    })
                }

                // castle queenside
                if(this.firstMove && 
                   board[index(x - 1, y)].type == PieceTypes.NONE &&
                   board[index(x - 2, y)].type == PieceTypes.NONE &&
                   board[index(x - 3, y)].type == PieceTypes.NONE &&
                   board[index(x - 4, y)].type == PieceTypes.ROOK &&
                   board[index(x - 4, y)].firstMove) {
                    ret.push(<Move>{
                        x: x - 2,
                        y: y,
                        type: MoveType.CASTLE 
                    })
                }
                break;
                
        }

        return ret
    }
}
