
let board: Board;

function setup() {
  const c = createCanvas(800, 900);
  c.parent('container')

  board = new Board();
}

function draw() {
  background(0);

  board.draw();
  noLoop();
}


function mousePressed(e: MouseEvent) {
    if(e.type !== "mousedown") {
      return 
    }

    const c = document.querySelector("canvas");
    const rect = c.getBoundingClientRect();

    let x = Math.abs(Math.round(((e.x - rect.x)/100)-0.5));
    let y = Math.abs(Math.round(((e.y - rect.y)/100)-0.5));

    board.clicked(x, y);
    loop();
}


