

const index = (x: number,y: number):number => {
    
    return x* 8 + y;
};

const isOutOfBounds = (x: number, y: number): boolean => {
    if(x < 0 || x > 7) {
        return true
    }

    if(y < 0 || y > 7) {
        return true 
    }

    return false
}

enum MoveType {
    NORMAL,
    TAKE,
    CASTLE,
    // TODO: Implement en passante
    // passantable flag on pawns, which is checked by the other pawns when standing next to them
    EN_PASSANTE
}

interface Move {
    x: number,
    y: number,
    type: MoveType 
}


interface Pos {
    x: number,
    y: number
}

