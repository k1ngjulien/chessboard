
enum BoardStates {
    WHITE = "WHITE",
    WHITE_PREVIEW = "WHITE_PREVIEW",
    WHITE_WON = "WHITE_WON",
    BLACK = "BLACK",
    BLACK_PREVIEW = "BLACK_PREVIEW",
    BLACK_WON = "BLACK_WON"
}

class Board{

    state : BoardStates
    board : Array<Piece>
    selected : Pos | null
    possibleMoves : Array<Move>
    collectedWhite: Array<Piece>
    collectedBlack: Array<Piece>

    constructor() {
        this.state = BoardStates.WHITE;
        this.board = new Array<Piece>(64);
        this.collectedBlack = new Array<Piece>(0);
        this.collectedWhite = new Array<Piece>(0);
        this.selected = null;
        this.possibleMoves =  new Array<Move>(0);


        for(let i = 0; i < 64; i ++) {
            this.board[i] = new Piece(PieceTypes.NONE, Color.NONE)
        }

        for(let x = 0; x < 8; x ++) {
            this.board[index(x,1)] = new Piece(PieceTypes.PAWN, Color.BLACK)
        }

        this.board[index(0,0)] = new Piece(PieceTypes.ROOK,   Color.BLACK);
        this.board[index(1,0)] = new Piece(PieceTypes.KNIGHT, Color.BLACK);
        this.board[index(2,0)] = new Piece(PieceTypes.BISHOP, Color.BLACK);
        this.board[index(3,0)] = new Piece(PieceTypes.QUEEN,  Color.BLACK);
        this.board[index(4,0)] = new Piece(PieceTypes.KING,   Color.BLACK);
        this.board[index(5,0)] = new Piece(PieceTypes.BISHOP, Color.BLACK);
        this.board[index(6,0)] = new Piece(PieceTypes.KNIGHT, Color.BLACK);
        this.board[index(7,0)] = new Piece(PieceTypes.ROOK,   Color.BLACK);

        for(let x = 0; x < 8; x ++) {
            this.board[index(x,6)] = new Piece(PieceTypes.PAWN, Color.WHITE)
        }

        this.board[index(0,7)] = new Piece(PieceTypes.ROOK,   Color.WHITE);
        this.board[index(1,7)] = new Piece(PieceTypes.KNIGHT, Color.WHITE);
        this.board[index(2,7)] = new Piece(PieceTypes.BISHOP, Color.WHITE);
        this.board[index(3,7)] = new Piece(PieceTypes.QUEEN,  Color.WHITE);
        this.board[index(4,7)] = new Piece(PieceTypes.KING,   Color.WHITE);
        this.board[index(5,7)] = new Piece(PieceTypes.BISHOP, Color.WHITE);
        this.board[index(6,7)] = new Piece(PieceTypes.KNIGHT, Color.WHITE);
        this.board[index(7,7)] = new Piece(PieceTypes.ROOK,   Color.WHITE);
    }

    getPiece(x: number, y: number) : Piece {
        return this.board[x*8 + y] 
    }
    
    setPiece(x: number, y: number, piece: Piece) {
        this.board[x*8 + y]  = piece
    }

    draw() {
        strokeWeight(0)

        // board
        let white = false
        for(let y = 0; y < 8; y++) {
            for(let x = 0; x < 8; x++) {
                white = !white;

                if(white) {
                    fill(255);
                }
                else {
                    fill(0) 
                }

                rect(100*x, 100*y, 100, 100)
            } 

            white = !white;
        }


        if(this.selected) {
            fill(0,255,0) 
            rect(100*this.selected.x, 100*this.selected.y, 100, 100)
        }
 
        // possible moves
        for(let m of this.possibleMoves) {
            switch(m.type){ 
                case MoveType.NORMAL:
                    fill(0,0,255, 128);
                    break;
                case MoveType.TAKE:
                    fill(255,0,0, 128);
                    break;
                case MoveType.CASTLE:
                    fill(255,255,0, 128);
                    break;
            }
            rect(100*m.x, 100*m.y, 100, 100);
        }
        
        // pieces
        for(let y = 0; y < 8; y++) {
            for(let x = 0; x < 8; x++) {
                this.board[x*8 + y].draw(x*100,  y*100);
            } 
        }

        // stats
        let x = 0;
        let y = 800;        
        // black pieces taken
        for(let piece of this.collectedBlack) {
            piece.draw(x, y, 0.5);
            x += 30;
        }
        // white pieces taken
        x = 0;
        y = 850;
        for(let piece of this.collectedWhite) {
            piece.draw(x, y, 0.5);
            x += 30;
        }

        // current player
        fill(255);
        textSize(20)
        text("Current Player:", 650, 820)
        switch(this.state) {
            case BoardStates.WHITE: 
            case BoardStates.WHITE_PREVIEW: 
                text("White", 700, 850);
                break;

            case BoardStates.BLACK:
            case BoardStates.BLACK_PREVIEW:
                text("Black", 700, 850);
                break;

            case BoardStates.WHITE_WON:
                text("White Won!", 650, 850);
                break;

            case BoardStates.BLACK_WON:
                text("Black Won!", 650, 850);
                break;
        }

    
    }


    clicked(x: number, y: number) {

        let nextState = this.state;
        switch(this.state) {
            case BoardStates.WHITE:
                if(this.getPiece(x, y).color != Color.WHITE) {
                    break; 
                }
                this.selected = <Pos>{x,y};
                this.possibleMoves = this.getPiece(x, y).getPossibleMoves(x,y,this.board)
                nextState = BoardStates.WHITE_PREVIEW;
                break;

            case BoardStates.BLACK:
                if(this.getPiece(x, y).color != Color.BLACK) {
                    break; 
                }
                this.selected = <Pos>{x,y};
                this.possibleMoves = this.getPiece(x, y).getPossibleMoves(x,y,this.board)
                nextState = BoardStates.BLACK_PREVIEW;
                break;
            
            case BoardStates.BLACK_PREVIEW: {
                let move = this.possibleMoves.find((move: Move): boolean => move.x == x && move.y == y)

                // deselect piece
                // clicking on the piece again or somewhere else
                if((this.selected.x === x && this.selected.y === y) || !move) {
                    this.selected = null; 
                    this.possibleMoves =  [];
                    nextState = BoardStates.BLACK;
                    break;
                }
                
                // take move
                if(move.type == MoveType.TAKE) {
                    let piece = this.getPiece(x, y);
                    this.collectedBlack.push(piece)  
                    this.collectedBlack.sort((a, b) => a.type - b.type)
                    this.setPiece(x, y, new Piece(PieceTypes.NONE)) 
                    
                    if(piece.type == PieceTypes.KING) {

                        this.move(index(this.selected.x, this.selected.y), index(x,y));
                        this.selected = null; 
                        this.possibleMoves =  [];
                        nextState = BoardStates.BLACK_WON;
                        break;
                    }
                }

                if(move.type == MoveType.CASTLE) {
                    // kingside castle
                    if(move.x == 6) {
                        // move rook
                        this.move(index(7, 0), index(5, 0)) 
                    }

                    // queenside castle
                    if(move.x == 2) {
                        // move rook
                        this.move(index(0, 0), index(3, 0)) 
                    }
                }

                this.move(index(this.selected.x, this.selected.y), index(x,y));

                this.selected = null; 
                this.possibleMoves =  [];
                nextState = BoardStates.WHITE;
                break;
            }

            case BoardStates.WHITE_PREVIEW: {
                let move = this.possibleMoves.find((move: Move): boolean => move.x == x && move.y == y)

                // deselect piece
                // clicking on the piece again or somewhere else
                if((this.selected.x === x && this.selected.y === y) || !move) {
                    this.selected = null; 
                    this.possibleMoves =  [];
                    nextState = BoardStates.WHITE;
                    break;
                }
                
                // take move
                if(move && move.type == MoveType.TAKE) {
                    let piece = this.getPiece(x, y);
                    this.collectedWhite.push(piece)  
                    this.collectedWhite.sort((a, b) => a.type - b.type)
                    this.setPiece(x, y, new Piece(PieceTypes.NONE)) 
                    
                    if(piece.type == PieceTypes.KING) {

                        this.move(index(this.selected.x, this.selected.y), index(x,y));
                        this.selected = null; 
                        this.possibleMoves =  [];
                        nextState = BoardStates.WHITE_WON;
                        break;
                    }
                }
                
                if(move.type == MoveType.CASTLE) {
                    // kingside castle
                    if(move.x == 6) {
                        // move rook
                        this.move(index(7, 7), index(5, 7)) 
                    }

                    // queenside castle
                    if(move.x == 2) {
                        // move rook
                        this.move(index(0, 7), index(3, 7)) 
                    }
                }

                this.move(index(this.selected.x, this.selected.y), index(x,y));
                this.selected = null; 
                this.possibleMoves =  [];
                nextState = BoardStates.BLACK;
                break;
            }
        }
        
        this.state = nextState

    }

    move(from: number, to: number) {
        this.board[from].firstMove = false;
        // swap pieces
        [this.board[from], this.board[to]] = [this.board[to], this.board[from]]; 

    }
}
